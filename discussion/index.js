console.log("Hello, World!");

/*
	camelCasing - the first letter of the second word is capitalize
	ctrl + shift + / - shortcut for multiple commenting
*/

let myVariable;
console.log(myVariable);

// console.log(hello);

let	productName = 'desktop computer';
console.log(productName);

let	productPrice = 18999;
console.log(productPrice);

const interest = 3.539;
console.log(interest);

productName = "Laptop";
console.log(productName);

let supplier;
supplier = 'John Smith Tradings';

// Multiple variable declaration
let productCode = 'DC017', productBrand = 'Dell';
console.log(productCode, productBrand);

let country = "Philippines";
let province = "Metro Manila";

let fullAddress = province + ", " + country;
console.log(fullAddress);

let greeting = "I live in the " + country;
console.log(greeting);

// Escape Character
let mailAddress = 'Metro Manila\n\nPhilippines'
console.log(mailAddress);

let message = "John's employees went home early"
console.log(message);

let headcount = 32;
console.log(headcount);

let grade = 98.7;
console.log(grade);

let planetDistance = 2e10;
console.log(planetDistance);
//==> 20000000000

console.log("John's grade last quarter is " + 98.7);
//==> John's grade last quarter is 98.7

let isMarried = true;
let inGoodCunduct = false;
console.log("isMarried: " + isMarried); //==> isMarried: true
console.log("inGoodCunduct: " + inGoodCunduct);//==> inGoodCunduct: false

//Array - are special kind of data type that's used to store multiple values;
let grades = [98.7, 92.1, 90.2, 94.6];
console.log(grades);
//==> [98.7, 92.1, 90.2, 94.6]

//Objects - another special kind of data type that's used to mimic real world objects/items;
let person = {
	fullName : 'Juan Dela Cruz',
	age : 35,
	isMarried : false,
	contact: ["09171234567", "81234 567"],
	Address : {
		houseNumber : "3456",
		city : "Manila"
	}
};
console.log(person);

let myGrades = {
	firstGrading : 98.7,
	secondGrading : 92.1,
	thirdGrading : 90.2,
	fourthGrading : 94.6
};

console.log(myGrades);

console.log(typeof myGrades);

//Null - used to intentionally express the absence of a value in a variable declaration/initialization.
let spouce = null;
let myNumber = 0;
let myString = "";

let fullName;
console.log(fullName);